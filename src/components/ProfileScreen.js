import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Alert, BackHandler, ToastAndroid} from 'react-native';

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.onBack = this.onBack.bind(this);
        this.didFocus = props.navigation.addListener('didFocus', payload =>
            BackHandler.addEventListener('hardwareBackPress', this.onBack)
        );
        this.state = {
            editing: false,
        };
    
    }
    static navigationOptions = {
        title: 'Profile Screen'
    }

    componentDidMount() {

        ToastAndroid.showWithGravityAndOffset('Click on the below button to make editing state OFF and ON', ToastAndroid.LONG, ToastAndroid.TOP, 25, 10)

        this.willBlur = this.props.navigation.addListener('willBlur', payload =>
        BackHandler.removeEventListener('hardwareBackPress', this.onBack)
        );
    }

    componentWillUnMount() {
        this.didFocus.remove();
        this.willBlur.remove();
        BackHandler.removeEventListener('hardwareBackPress', this.onBack);
    }
    
    onBack = () => {

        if (this.state.editing) {
            console.log('editing: ',this.state.editing)

            Alert.alert(
                "You're still editing!",
                "Are you sure you want to go home with your edits not saved?",
                [
                    { text: "Keep Editing", onPress: () => { }, style: 'cancel' },
                    { text: "Go Home", onPress: () => this.props.navigation.goBack() },
                ],
                { cancelable: false }
            );
            return true;
        }
        return false;
    };

    render() {
        const { editing } = this.state
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => (this.setState({ editing: !editing }))}
                >
                    <Text>Toggle Editing {editing ? 'Off' : 'On'}</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

export default ProfileScreen;