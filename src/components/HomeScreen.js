import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Home'
    };

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.push('Profile')}
                >
                    <Text>Go to Profile Screen</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default HomeScreen;