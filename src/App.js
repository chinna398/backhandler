import React from "react";

import Routes from './RouteScreen';

export default class App extends React.Component {
  render() {
    return <Routes />;
  }
}