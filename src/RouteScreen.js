import {

    createStackNavigator,
    createAppContainer
} from 'react-navigation'

import HomeScreen from './components/HomeScreen';
import ProfileScreen from './components/ProfileScreen';

const HomeAndProfileNav = createStackNavigator(
    {
        Home:{
            screen: HomeScreen
        },
        Profile:{
            screen: ProfileScreen
        }
    }
)

export default createAppContainer(HomeAndProfileNav)